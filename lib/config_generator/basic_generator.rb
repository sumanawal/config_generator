# frozen_string_literal: true

# Generator for the basic elements.
# Currently we categorized redis, sidekiq and hutch 
# as a basic tools that need generator.

# require 'redis_generator'
# require 'hutch_generator'
# require 'sidekiq_generator'
