# frozen_string_literal: true
module ConfigGenerator
  class Railtie < ::Rails::Railtie
    generators do |app|
      Rails::Generators.configure! app.config.generators
      Rails::Generators.hidden_namespaces.uniq!
    end
  end
end
