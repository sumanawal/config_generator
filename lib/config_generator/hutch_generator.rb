# frozen_string_literal: true

# Generator file for the redis
module ConfigGenerator
  # :no-doc:
  class HutchMessaging < Rails::Generators::Base
    source_root File.expand_path('../templates/hutch', __FILE__)

    def create_file_app_version
      copy_file 'hutch.example.yml', 'config/hutch.example.yml'
      copy_file 'hutch.rb', 'config/initializers/hutch.rb'
    end
  end
end
