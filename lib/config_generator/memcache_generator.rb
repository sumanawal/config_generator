# frozen_string_literal: true

# Generator file for the redis
module ConfigGenerator
  # :no-doc:
  class MemcacheStorage < Rails::Generators::Base
    source_root File.expand_path('../templates/redisdb', __FILE__)

    def create_file_app_version
      copy_file 'memcache.example.yml', 'config/memcache.example.yml'
      copy_file 'memcache.rb', 'config/initializers/memcache.rb'
    end
  end
end
