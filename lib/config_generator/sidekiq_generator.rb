# frozen_string_literal: true

# Generator file for the sidekiq
module ConfigGenerator
  # :no-doc:
  class SidekiqConfig < Rails::Generators::Base
    source_root File.expand_path('../templates/sidekiq', __FILE__)

    def create_file_app_version
      copy_file 'sidekiq.example.yml', 'config/sidekiq.example.yml'
      copy_file 'sidekiq.rb', 'config/initializers/sidekiq.rb'
      directory 'sidekiq_workers', 'app/sidekiq_workers'
    end
  end
end
