# Be sure to restart your server when you modify this file.

# Rails.application.config.session_store :cookie_store, key: '_cfclientapp_session'

require 'action_dispatch/middleware/session/dalli_store'

memcache_config = Rails.application.config_for(:memcache)

Rails.application.config.session_store(:dalli_store, memcache_config)
