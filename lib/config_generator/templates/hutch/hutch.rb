# frozen_string_literal: true

module Hutch
  # establish connection with rabbit_mq server
  module Connector
    def self.establish_connection
      Hutch::Config.initialize(config)
    end

    def self.config
      Rails.application.config_for(:hutch)
    end
  end
end

Hutch::Connector.establish_connection
