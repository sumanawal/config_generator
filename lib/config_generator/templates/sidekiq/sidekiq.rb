require 'sidekiq/web'
# require 'sidekiq/cron/web'
# https://github.com/mperham/sidekiq/issues/2487
# We dont have setup for the secret_token
# Initialize secret_token and then uncomment if there is error in prod env.
# Sidekiq::Web.set :session_secret, Rails.application.secrets[:secret_token]
# Sidekiq::Web.set :sessions, Rails.application.config.session_options

redis_config = Rails.application.config_for(:redis)
sidekiq_config = Rails.application.config_for(:sidekiq)
Sidekiq::Web.set :sessions, domain: APP_CONFIG['host']

Sidekiq.configure_server do |config|
  config.failures_default_mode = :exhausted
  config.redis = { url: "redis://#{redis_config[:host]}:#{redis_config[:port]}/#{sidekiq_config[:database]}", namespace: 'sidekiq' }
end

Sidekiq.configure_client do |config|
  config.redis = { url: "redis://#{redis_config[:host]}:#{redis_config[:port]}/#{sidekiq_config[:database]}", namespace: 'sidekiq' }
end

Sidekiq::Web.use Rack::Auth::Basic do |username, password|
  username == sidekiq_config[:username] && Digest::SHA1.hexdigest(password) == sidekiq_config[:password]
end

Sidekiq.default_worker_options = { 'queue' => 'default', 'retry' => 3, 'backtrace' => true }

Dir["#{Rails.root}/app/sidekiq_workers/**/*.rb"].each do |filename|
  require "#{filename}"
end
