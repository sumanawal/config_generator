# frozen_string_literal: true

# Generator file for the redis
module ConfigGenerator
  # :no-doc:
  class Redisdb < Rails::Generators::Base
    source_root File.expand_path('../templates/redisdb', __FILE__)

    def create_file_app_version
      copy_file 'redis.example.yml', 'config/redis.example.yml'
      copy_file 'redisdb.rb', 'config/initializers/redisdb.rb'
    end
  end
end
